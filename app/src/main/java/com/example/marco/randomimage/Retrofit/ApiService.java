package com.example.marco.randomimage.Retrofit;

import com.example.marco.randomimage.Pojo.flickr.FlickrFeed;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

//https://api.flickr.com/services/feeds/photos_public.gne?format=json&nojsoncallback=1
public interface ApiService {
    @GET("feeds/photos_public.gne?format=json&nojsoncallback=1")
    Call<FlickrFeed> getImageUrl();
}
