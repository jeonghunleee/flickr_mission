package com.example.marco.randomimage;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.marco.randomimage.Pojo.flickr.FlickrFeed;
import com.example.marco.randomimage.Retrofit.ApiService;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    static final String URL = "https://api.flickr.com/services/";
    Point thisScreen;
    int x,y;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btn=(Button)findViewById(R.id.btn_start);
        ImageView img =(ImageView)findViewById(R.id.imageView);
        img.setAdjustViewBounds(true);
        thisScreen=getScreenSize(this);
        img.getLayoutParams().height=thisScreen.y;
        img.getLayoutParams().width=thisScreen.x;
        img.requestLayout();
        btn.setOnClickListener(v -> getImageFromFlickr());
        Log.i("xxx",Integer.toString(thisScreen.x));
        Log.i("yyy",Integer.toString(thisScreen.y));

        //getImageFromFlickr();

    }

    public void doPicasso(ImageView img,String url){
        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.btn_more)
                .into((ImageView)findViewById(R.id.imageView));

    }

    public Point getScreenSize(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return  size;
    }

    public void getImageFromFlickr() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiService cloverService = retrofit.create(ApiService.class);
        Call<FlickrFeed> call = (Call<FlickrFeed>) cloverService.getImageUrl();
        call.enqueue(new FlickrCustom());
    }

    class FlickrCustom implements Callback<FlickrFeed> {
        @Override
        public void onResponse(Call<FlickrFeed> call, Response<FlickrFeed> response) {
            if (response.isSuccessful()) {

                FlickrFeed result = response.body();
                String url = result.getItems().get(0).getMedia().getM();
                Toast.makeText(getApplicationContext(),url,Toast.LENGTH_LONG).show();
                ImageView img=(ImageView)findViewById(R.id.imageView);
                doPicasso(img,url);
            } else {

            }
        }
        @Override
        public void onFailure(Call<FlickrFeed> call, Throwable t) {
        }
    }

}
